package com.JustusFluegel.JDBC

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException

class JdbcConnection(username: String, password: String, domain: String, database: String) {

    private var connection: Connection? = null

    init {


        Thread(Runnable {
            try {
                connection = DriverManager.getConnection("jdbc:mysql://$domain/$database?verifyServerCertificate=false&useSSL=true&requireSSL=true", username, password)
            } catch (e: SQLException) {
                e.printStackTrace()
            }
        }).start()

    }


    fun runSQL(SQL: String): ResultSet? {

        var res: ResultSet? = null


        try {
            while (connection == null) {
                Thread.sleep(10)
            }

            res = connection!!.createStatement().executeQuery(SQL)
        } catch (e: SQLException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }


        return res
    }

    fun runSQLUpdate(SQL: String) {

        try {
            while (connection == null) {
                Thread.sleep(10)
            }
            connection!!.createStatement().executeUpdate(SQL)
        } catch (e: SQLException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }


    }

}
