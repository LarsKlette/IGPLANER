package com.JustusFluegel.chat;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.JustusFluegel.adapters.CustomAdapter;

import java.util.ArrayList;

public class SelectContact {

    private boolean disablesystemclick = false;

    private Spinner selectspinner;
    private TextView selectedtextview;
    private Chat chati;
    private boolean Selected = false;
    private Activity currentactivity;
    private ArrayList<String> allcontacts = new ArrayList<>();
    public ArrayList<String> contacts = new ArrayList<>();
    private int DropDownRessource;
    private View[] openbuttons;
    private View usercontainer;
    private int accontact = 0;
    private String contact = null;

    public SelectContact(final Spinner select, TextView selected, Activity activity, final View container, View... open) {

        currentactivity = activity;

        selectspinner = select;
        selectedtextview = selected;

        String contact = "";

        SharedPreferences pref = activity.getSharedPreferences("Contacts", 0);
        while (!contact.equals("7162a9d9-bbdc-454a-9697-787c282e02f5")) {
            contact = pref.getString("Contact:", "7162a9d9-bbdc-454a-9697-787c282e02f5");
            if (!contact.equals("7162a9d9-bbdc-454a-9697-787c282e02f5")) {
                contacts.add(contact);
            }
        }

        openbuttons = open;
        usercontainer = container;

    }

    public void show() {

        if (contacts.size() != 0) {

            String[] array = new String[contacts.size()];
            array = contacts.toArray(array);
            final CustomAdapter dropdownadapter = new CustomAdapter(currentactivity, android.R.layout.simple_spinner_item, array, 0);
            dropdownadapter.setDropDownViewResource(DropDownRessource);
            selectspinner.setAdapter(dropdownadapter);

            for (View view : openbuttons) {

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            selectspinner.setDropDownWidth(usercontainer.getWidth());
                        }

                        selectspinner.performClick();

                        selectspinner.setSelection(0);
                        selectspinner.getSelectedView().setVisibility(View.INVISIBLE);

                    }
                });

            }

            selectspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (disablesystemclick) {

                        String contact = contacts.get(i);

                        setContact(contact);

                        String[] array = new String[contacts.size()];
                        array = contacts.toArray(array);
                        final CustomAdapter dropdownadapter = new CustomAdapter(currentactivity, android.R.layout.simple_spinner_item, array , 0);
                        dropdownadapter.setDropDownViewResource(DropDownRessource);
                        selectspinner.setAdapter(dropdownadapter);

                        disablesystemclick = false;

                    } else {
                        disablesystemclick = true;
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }

    }

    public void selectChat(Chat chat) {
        Selected = true;
        chati = chat;
    }

    public void addContact(String contact) {

        allcontacts.add(contact);

    }

    public void addContactApply() {

        SharedPreferences pref = currentactivity.getSharedPreferences("Contacts", 0);
        setContact(pref.getString("ActualContact", allcontacts.get(0)));


    }

    public void save() {

        SharedPreferences pref = currentactivity.getSharedPreferences("Contacts", 0);
        SharedPreferences.Editor prefeditor = pref.edit();

        for (int i = 0; i < contacts.size(); i++) {
            prefeditor.putString("Contact:" + Integer.toString(i), contacts.get(i));
        }

        prefeditor.apply();
    }

    private void setContact(String contact) {

        contacts.clear();
        contacts.addAll(allcontacts);

        for (int i = 0; i < contacts.size(); i++) {

            if (contacts.get(i).equals(contact)) {

                accontact = i;

            }

        }

        selectedtextview.setText(contacts.get(accontact));
        contact = contacts.get(accontact);

        try {
            tellChat(contacts.get(accontact));
        } catch (ChatNotSelectedException e) {
            e.printStackTrace();
        }


        contacts.remove(accontact);



    }

    public void setDropDownViewResource(int Resource) {

        DropDownRessource = Resource;

    }

    private void tellChat(String name) throws ChatNotSelectedException {

        if (Selected) {

            chati.selectContact(name);

        } else {
            throw new ChatNotSelectedException();
        }

    }

    public String getContact(){

        return contact;

    }


}

