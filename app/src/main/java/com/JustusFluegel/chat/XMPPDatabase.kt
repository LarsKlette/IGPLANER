package com.JustusFluegel.chat

import android.util.Log

import com.JustusFluegel.JDBC.JdbcConnection
import com.JustusFluegel.igplaner.Variables

import java.sql.ResultSet
import java.sql.SQLException
import java.util.ArrayList

class XMPPDatabase {

    private val data = ArrayList<ArrayList<String>>()
    private var selected: Boolean = false

    internal var connection: JdbcConnection

    init {

        data.add(ArrayList())
        data.add(ArrayList())
        data.add(ArrayList())

        connection = JdbcConnection(Variables.SqlUsername, Variables.SqlPassword, Variables.SqlDomain, "klasseninformation")

        Thread(Runnable {
            val res = connection.runSQL("SELECT * FROM 7f")
            try {

                while (res!!.next()) {

                    Log.d("SQL", res.getString(1))
                    Log.d("SQL", res.getString(2))
                    Log.d("SQL", res.getString(3))
                    Log.d("SQL", res.getString(4))
                    Log.d("SQL", res.getString(5))

                }
            } catch (ignored: SQLException) {
            }
        }).start()

    }

    fun setSelectContacts(contact: SelectContact) {

        selected = true

        data[0].addAll(contact.contacts)


    }

}
