@file:Suppress("PackageName")
package com.JustusFluegel.chat

class ChatNotSelectedException : Exception {

    constructor() {}
    constructor(message: String) : super(message) {}

}
