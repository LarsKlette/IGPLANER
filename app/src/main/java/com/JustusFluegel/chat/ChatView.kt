@file:Suppress("PrivatePropertyName", "PackageName", "LocalVariableName")
package com.JustusFluegel.chat

import android.app.Activity
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.Space
import android.widget.TextView
import com.JustusFluegel.general.Functions
import java.util.*

class ChatView(private val activity: Activity, private var chatview: ScrollView?, private var chat_bubble_color: Int, private val name_color: Int) {

    private val chat = ArrayList<ArrayList<Any>>()
    private var chat_bubble_color_myself: Int = 0
    private var bubble_radius: Int = 0
    private var bubble_padding: Int = 0
    private var bubble_margin: Int = 0

    init {
        chat_bubble_color_myself = chat_bubble_color

        chat.add(ArrayList()) //add member "0"
        chat.add(ArrayList()) //add text / image / file "1"
        chat.add(ArrayList()) //add Person names

        bubble_radius = Functions.dpToPx(10, activity)
        bubble_padding = Functions.dpToPx(8, activity)
        bubble_margin = Functions.dpToPx(4, activity)
    }

    fun setBubbleColor(ColorRessource: Int) {

        chat_bubble_color = ColorRessource

    }

    fun setOwnChatBubbleColor(ColorRessource: Int) {

        chat_bubble_color_myself = ColorRessource

    }

    fun setChatView(view: ScrollView) {

        chatview = view

    }

    fun apply() {

        chatview!!.removeAllViews()

        val content = LinearLayout(activity)
        content.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        content.orientation = LinearLayout.VERTICAL

        for (i in 0 until chat[0].size) {

            val myself = chat[0][i] as Boolean

            val element_content = chat[1][i] as View
            element_content.id = findId()

            val element_view = LinearLayout(activity)
            element_view.orientation = LinearLayout.VERTICAL

            if (chat[2][i] != "") {

                val pers = TextView(activity)
                pers.textSize = 8f
                pers.text = chat[2][i] as String
                pers.setTextColor(ContextCompat.getColor(activity.applicationContext , name_color))

                pers.setPadding(0, 0, 0, Functions.dpToPx(1, activity))

                element_view.addView(pers)

            }

            element_view.addView(element_content)

            element_view.setPadding(bubble_padding, bubble_padding, bubble_padding, bubble_padding)



            if (myself) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                    element_view.background = chatbubbleright(chat_bubble_color_myself)
                else
                    @Suppress("DEPRECATION")
                    element_view.setBackgroundDrawable(chatbubbleright(chat_bubble_color_myself))

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                    element_view.background = chatbubbleleft(chat_bubble_color)
                else
                    @Suppress("DEPRECATION")
                    element_view.setBackgroundDrawable(chatbubbleleft(chat_bubble_color))

            }

            val element = LinearLayout(activity)
            val element_params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            element_params.setMargins(0, bubble_margin, 0, 0)
            element.layoutParams = element_params
            element.orientation = LinearLayout.HORIZONTAL

            if (myself) {

                val space = Space(activity)

                space.layoutParams = LinearLayout.LayoutParams(0, 0, 1.0f)

                element.addView(space)

            }
            element.addView(element_view)
            if (!myself) {

                val space = Space(activity)

                space.layoutParams = LinearLayout.LayoutParams(0, 0, 1.0f)

                element.addView(space)

            }

            content.addView(element)

        }

        chatview!!.addView(content)

    }

    fun append(apply: Boolean, myself: Boolean, obj: Any, person: String = "") {

        chat[2].add(if (myself) "" else person)

        chat[0].add(myself)
        chat[1].add(obj)

        if (apply) {
            apply()
        }
    }

    fun setBubbleRadiusDp(dp: Int) {

        bubble_radius = Functions.dpToPx(dp, activity)

    }

    @Deprecated("")
    fun setBubbleRadius(px: Int) {

        bubble_radius = px

    }

    fun setBubblePaddingDp(dp: Int) {

        bubble_padding = Functions.dpToPx(dp, activity)

    }

    @Deprecated("")
    fun setBubblePadding(px: Int) {

        bubble_padding = px

    }

    fun setBubbleMarginDp(dp: Int) {

        bubble_margin = Functions.dpToPx(dp, activity)

    }

    @Deprecated("")
    fun setBubbleMargin(px: Int) {

        bubble_margin = px

    }

    private fun findId(): Int {
        var v: View? = activity.findViewById(id)
        while (v != null) {
            v = activity.findViewById(++id)
        }
        return id++
    }

    private fun chatbubbleright(backgroundColor: Int): GradientDrawable {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.cornerRadii = floatArrayOf(bubble_radius.toFloat(), bubble_radius.toFloat(), bubble_radius.toFloat(), bubble_radius.toFloat(), 0f, 0f, bubble_radius.toFloat(), bubble_radius.toFloat())
        shape.setColor(ContextCompat.getColor(activity.applicationContext ,backgroundColor))
        return shape
    }

    private fun chatbubbleleft(backgroundColor: Int): GradientDrawable {
        val shape = GradientDrawable()
        shape.shape = GradientDrawable.RECTANGLE
        shape.cornerRadii = floatArrayOf(bubble_radius.toFloat(), bubble_radius.toFloat(), bubble_radius.toFloat(), bubble_radius.toFloat(), bubble_radius.toFloat(), bubble_radius.toFloat(), 0f, 0f)
        shape.setColor(ContextCompat.getColor(activity.applicationContext ,backgroundColor))
        return shape
    }

    companion object {
        private var id = 1
    }

}
