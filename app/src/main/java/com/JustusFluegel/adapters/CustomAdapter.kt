@file:Suppress("PackageName")
package com.JustusFluegel.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class CustomAdapter(context: Context, textViewResourceId: Int, objects: Array<String>, private val hidingItemIndex: Int) : ArrayAdapter<String>(context, textViewResourceId, objects) {

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val v: View
        if (position == hidingItemIndex) {
            val tv = TextView(context)
            tv.visibility = View.GONE
            v = tv
        } else {
            v = super.getDropDownView(position, null, parent)
        }
        return v
    }

}