@file:Suppress("PackageName", "LocalVariableName", "UnnecessaryVariable", "FunctionName", "MoveLambdaOutsideParentheses")
package com.JustusFluegel.general

import android.app.Activity
import android.os.Environment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.util.TypedValue
import com.JustusFluegel.igplaner.R
import java.io.File
import java.io.IOException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.text.SimpleDateFormat
import java.util.*

object Functions {

    val salt: ByteArray
        @Throws(NoSuchAlgorithmException::class)
        get() {
            val sr = SecureRandom.getInstance("SHA1PRNG")
            val salt = ByteArray(16)
            sr.nextBytes(salt)
            Log.d("Salt", Arrays.toString(salt))
            return salt
        }

    fun setColors(light: Boolean, activity: Activity) {

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        val DesignPreferencesEditor = DesignPreferences.edit()

        DesignPreferencesEditor.putBoolean("LightTheme", light)
        DesignPreferencesEditor.apply()

        Menue.ApplyColors(activity)

    }

    fun getDesignTextcolor(activity: Activity): Int {

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        val light = DesignPreferences.getBoolean("LightTheme", false)

        return if (light) ContextCompat.getColor(activity.applicationContext,R.color.textcolor_inverted) else ContextCompat.getColor(activity.applicationContext,R.color.textcolor)
    }

    fun getDesignTextcolorDark(activity: Activity): Int {

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        val light = DesignPreferences.getBoolean("LightTheme", false)

        return if (light) ContextCompat.getColor(activity.applicationContext,R.color.textcolorDark_inverted) else ContextCompat.getColor(activity.applicationContext,R.color.textcolorDark)
    }

    fun getDesignBackgroundImage(activity: Activity): Int {

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        val light = DesignPreferences.getBoolean("LightTheme", false)


        return if (light) R.drawable.igpbackground_inverted else R.drawable.igpbackground

    }

    fun getDesignBackground(activity: Activity): Int {

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        val light = DesignPreferences.getBoolean("LightTheme", false)

        return if (light) ContextCompat.getColor(activity.applicationContext,R.color.background_inverted) else ContextCompat.getColor(activity.applicationContext,R.color.background)
    }

    fun getDesignMenue(activity: Activity): Int {

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        val light = DesignPreferences.getBoolean("LightTheme", false)

        return if (light) ContextCompat.getColor(activity.applicationContext,R.color.menu_hell) else ContextCompat.getColor(activity.applicationContext,R.color.menu)
    }

    fun getDesignSppinnerStyle(activity: Activity): Int {

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        val light = DesignPreferences.getBoolean("LightTheme", false)

        return if (light) R.layout.spinner_custom_design_light_1 else R.layout.spinner_custom_design_dark_1
    }

    @Throws(IOException::class)
    fun createImageFile(activity: Activity): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.GERMANY).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        return image
    }

    fun dpToPx(dp: Int, activity: Activity): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), activity.resources.displayMetrics).toInt()
    }

    @Throws(NumberFormatException::class)
    fun stringToArryay(string: String): ByteArray {
        val result = ArrayList<Byte>()

        val segments = string.split(",".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

        for (segment in segments) {

            result.add(java.lang.Byte.valueOf(segment))

        }

        val Result: Array<Byte?>

        Result = result.toTypedArray()

        val resultnoobj = ByteArray(Result.size)

        var j = 0

        for (b in Result) {
            resultnoobj[j++] = b!!
        }

        return resultnoobj
    }

}
