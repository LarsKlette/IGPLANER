@file:Suppress("PackageName")

package com.JustusFluegel.general

import android.app.Activity
import android.os.AsyncTask
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.widget.Toast
import com.JustusFluegel.html.ServerHttpConnection
import com.JustusFluegel.igplaner.Anmelden
import com.JustusFluegel.igplaner.Variables
import com.JustusFluegel.security.Functions
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.*

object Initialize {

    private class AsyncCheck : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {

            try {

                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)


                Class.forName("com.mysql.jdbc.Driver")
                val conn: Connection = DriverManager.getConnection("jdbc:mysql://" + Variables.SqlDomain + "/dienste?verifyServerCertificate=false&useSSL=true&requireSSL=true",
                        Variables.SqlUsername, Variables.SqlPassword)

                conn.createStatement()


            } catch (e: ClassNotFoundException) {
                e.printStackTrace()

                // Usernames += e.toString();
            } catch (e: SQLException) {
                e.printStackTrace()
            }

            Variables.SqlServerOn = true

            return null
        }

        override fun onCancelled() {
            Variables.SqlServerOn = false
            super.onCancelled()
        }
    }

    private class AsyncLogin : AsyncTask<Activity, Void, Void>() {

        override fun doInBackground(vararg activities: Activity): Void? {

            val activity = activities[0]

            val anmeldedaten = activity.getSharedPreferences("Anmeldedaten", 0)

            try {

                if (anmeldedaten.getInt("Abgemeldet", 1) == 0) {

                    val user = anmeldedaten.getString("MyUsername", Integer.toString(Integer.MIN_VALUE))

                    val l = Anmelden.login(anmeldedaten.getString("MyPasword", Integer.toString(Integer.MAX_VALUE)), user)

                    if (l != "false") {
                        Variables.angemeldet = true
                        Variables.Klasse = Integer.parseInt(l)
                        Variables.Username = user.toString()
                    }
                }
            } catch (ignored: Exception) {
            }

            return null
        }


    }

    fun startInitialize(activity: Activity) {

        val asyncCheck = AsyncCheck()
        asyncCheck.execute()

        if (Anmelden.isConnected(activity.applicationContext)) {

            val asyncLogin = AsyncLogin()
            asyncLogin.execute(activity)

        } else {
            val preferences = activity.getSharedPreferences("Anmeldedaten", 0)
            val klasse = preferences.getInt("Klasse", Integer.MAX_VALUE)

            if (klasse != Integer.MAX_VALUE) {
                Variables.pseudoangemeldet = true

            }

        }


        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                if (Variables.angemeldet) {

                    val handler2 = Handler()

                    val conn = ServerHttpConnection("https://" + Variables.Domain + "/pass/pass_safe_auth.php")
                    handler2.postDelayed(object : Runnable {
                        override fun run() {

                            Thread(Runnable {

                                val preferences = activity.getSharedPreferences("Server Data", 0)

                                val pass = "72daff08-c397-4b6a-b2d1-a4c26d60eaa5"
                                var lpass = preferences.getString("ActPass", Functions.macAdress)
                                var luser = preferences.getString("ActUser", Functions.macAdress)

                                try {
                                    val arr = conn.sendmultipleTextForResultinArray(arrayOf("GetNDBPass", "mac", "pass", "lpass", "luser"), arrayOf("true", Functions.macAdress, pass, lpass, luser))

                                    Log.d("Server gives back", Arrays.toString(arr.toTypedArray()))

                                    if (arr.size == 2) {

                                        luser = arr[0]
                                        lpass = arr[1]

                                        val preferencesedit = preferences.edit()

                                        preferencesedit.putString("ActPass", lpass)
                                        preferencesedit.putString("ActUser", luser)

                                        Variables.SqlUsername = luser
                                        Variables.SqlPassword = lpass

                                        preferencesedit.apply()


                                    } else {
                                        Toast.makeText(activity.applicationContext, "Can't connect to server, maybe a wrong password? If you have internet and the arror stays, please re-register your device!", Toast.LENGTH_LONG).show()
                                    }


                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }


                            }).start()

                            handler2.postDelayed(this, 240000)
                        }
                    }, 0)

                } else
                    handler.postDelayed(this, 5)
            }
        }, 5)

    }


}
