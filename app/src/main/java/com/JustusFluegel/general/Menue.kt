@file:Suppress("PackageName", "LocalVariableName", "FunctionName")
package com.JustusFluegel.general


import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.util.TypedValue
import android.view.View
import android.widget.Button
import android.widget.Switch
import com.JustusFluegel.igplaner.*

object Menue {

    private var isOpen: Boolean? = null

    fun Menu(activity: Activity, keep: Boolean?) {

        ApplyColors(activity)

        val menu = activity.findViewById<ConstraintLayout>(R.id.menu)
        val menubutton = activity.findViewById<Button>(R.id.MenuButton)
        val menustundenplanbutton = activity.findViewById<Button>(R.id.StundenplanButton)
        val menustundenplanicon = activity.findViewById<Button>(R.id.StundenplanIcon)
        val menudienstebutton = activity.findViewById<Button>(R.id.DiensteButton)
        val menudiensteicon = activity.findViewById<Button>(R.id.DiensteIcon)
        val menuchaticon = activity.findViewById<Button>(R.id.ChatIcon)
        val menuchatbutton = activity.findViewById<Button>(R.id.ChatButton)
        val menusettingsbutton = activity.findViewById<Button>(R.id.SettingsButton)
        val menusettingsicon = activity.findViewById<Button>(R.id.SettingsIcon)
        val menuanmeldebutton = activity.findViewById<Button>(R.id.AnmeldeButton)
        val menuanmeldeicon = activity.findViewById<Button>(R.id.AnmeldeIcon)

        isOpen = false

        menu.maxWidth = dpToPx(40, activity)
        menu.minWidth = dpToPx(40, activity)

        menubutton.layoutParams = ConstraintLayout.LayoutParams(dpToPx(40, activity), menubutton.height)

        menustundenplanbutton.visibility = View.INVISIBLE
        menudienstebutton.visibility = View.INVISIBLE
        menuanmeldebutton.visibility = View.INVISIBLE
        menuchatbutton.visibility = View.INVISIBLE
        menusettingsbutton.visibility = View.INVISIBLE
        menu.visibility = View.VISIBLE

        val symbols = Typeface.createFromAsset(activity.assets, "Schriftart_Symbole/Symbols-Regular.ttf")
        menuchaticon.typeface = symbols


        if (Variables.angemeldet) {
            menuanmeldebutton.setText(R.string.text_abmelden)
        } else {
            menuanmeldebutton.setText(R.string.text_anmelden)
        }

        //<editor-fold desc="Menübutton">
        menubutton.setOnClickListener {
            if (isOpen!!) {

                isOpen = false

                menu.maxWidth = dpToPx(40, activity)
                menu.minWidth = dpToPx(40, activity)

                menubutton.layoutParams = ConstraintLayout.LayoutParams(dpToPx(40, activity), menubutton.height)

                menustundenplanbutton.visibility = View.INVISIBLE
                menudienstebutton.visibility = View.INVISIBLE
                menuanmeldebutton.visibility = View.INVISIBLE
                menuchatbutton.visibility = View.INVISIBLE
                menusettingsbutton.visibility = View.INVISIBLE


            } else {

                isOpen = true

                menu.maxWidth = dpToPx(193, activity)
                menu.minWidth = dpToPx(193, activity)

                menubutton.layoutParams = ConstraintLayout.LayoutParams(dpToPx(193, activity), menubutton.height)

                menustundenplanbutton.visibility = View.VISIBLE
                menudienstebutton.visibility = View.VISIBLE
                menuanmeldebutton.visibility = View.VISIBLE
                menuchatbutton.visibility = View.VISIBLE
                menusettingsbutton.visibility = View.VISIBLE


            }
        }
        //</editor-fold>

        menustundenplanbutton.setOnClickListener {
            if (activity.javaClass.simpleName != StundenplanLandscape::class.java.simpleName && activity.javaClass.simpleName != StundenplanLandscape::class.java.simpleName) {
                activity.startActivity(Intent(activity, StundenplanLandscape::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }

        menustundenplanicon.setOnClickListener {
            if (activity.javaClass.simpleName != StundenplanLandscape::class.java.simpleName && activity.javaClass.simpleName != StundenplanProitrait::class.java.simpleName) {
                activity.startActivity(Intent(activity, StundenplanLandscape::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }

        menuanmeldebutton.setOnClickListener {
            if (activity.javaClass.simpleName != Anmelden::class.java.simpleName) {
                activity.startActivity(Intent(activity, Anmelden::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }

        menuanmeldeicon.setOnClickListener {
            if (activity.javaClass.simpleName != Anmelden::class.java.simpleName) {
                activity.startActivity(Intent(activity, Anmelden::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }

        menudienstebutton.setOnClickListener {
            if (activity.javaClass.simpleName != Dienste::class.java.simpleName) {
                activity.startActivity(Intent(activity, Dienste::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }

        menudiensteicon.setOnClickListener {
            if (activity.javaClass.simpleName != Dienste::class.java.simpleName) {
                activity.startActivity(Intent(activity, Dienste::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }
        menuchaticon.setOnClickListener {
            if (activity.javaClass.simpleName != XMPP::class.java.simpleName) {
                activity.startActivity(Intent(activity, XMPP::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }
        menuchatbutton.setOnClickListener {
            if (activity.javaClass.simpleName != XMPP::class.java.simpleName) {
                activity.startActivity(Intent(activity, XMPP::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }
        menusettingsicon.setOnClickListener {
            if (activity.javaClass.simpleName != Settings::class.java.simpleName) {
                activity.startActivity(Intent(activity, Settings::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }
        menusettingsbutton.setOnClickListener {
            if (activity.javaClass.simpleName != Settings::class.java.simpleName) {
                activity.startActivity(Intent(activity, Settings::class.java))
                if ((!keep!!)) {
                    activity.finish()
                }
            }
        }

    }

    private fun dpToPx(dp: Int, activity: Activity): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), activity.resources.displayMetrics).toInt()
    }

    fun ApplyColors(activity: Activity) {

        val light: Boolean

        val DesignPreferences = activity.getSharedPreferences("Design", 0)
        light = DesignPreferences.getBoolean("LightTheme", false)


        val lightdesign = activity.findViewById<Switch>(R.id.light_design)
        if (lightdesign != null) {
            lightdesign.isChecked = light
        }


        val BackgroundColor = if (!light) ContextCompat.getColor(activity.applicationContext,R.color.menu) else ContextCompat.getColor(activity.applicationContext,R.color.menu_hell)
        val ForegroundColor = if (!light) ContextCompat.getColor(activity.applicationContext,R.color.textcolor) else ContextCompat.getColor(activity.applicationContext,R.color.textcolor_inverted)

        val menu = activity.findViewById<ConstraintLayout>(R.id.menu)
        val menubutton = activity.findViewById<Button>(R.id.MenuButton)
        val menustundenplanbutton = activity.findViewById<Button>(R.id.StundenplanButton)
        val menustundenplanicon = activity.findViewById<Button>(R.id.StundenplanIcon)
        val menudienstebutton = activity.findViewById<Button>(R.id.DiensteButton)
        val menudiensteicon = activity.findViewById<Button>(R.id.DiensteIcon)
        val menuchaticon = activity.findViewById<Button>(R.id.ChatIcon)
        val menuchatbutton = activity.findViewById<Button>(R.id.ChatButton)
        val menusettingsbutton = activity.findViewById<Button>(R.id.SettingsButton)
        val menusettingsicon = activity.findViewById<Button>(R.id.SettingsIcon)
        val menuanmeldebutton = activity.findViewById<Button>(R.id.AnmeldeButton)
        val menuanmeldeicon = activity.findViewById<Button>(R.id.AnmeldeIcon)


        menu.setBackgroundColor(BackgroundColor)

        menubutton.setTextColor(ForegroundColor)

        menustundenplanbutton.setTextColor(ForegroundColor)
        menustundenplanicon.setTextColor(ForegroundColor)

        menudienstebutton.setTextColor(ForegroundColor)
        menudiensteicon.setTextColor(ForegroundColor)

        menuchatbutton.setTextColor(ForegroundColor)
        menuchaticon.setTextColor(ForegroundColor)

        menusettingsbutton.setTextColor(ForegroundColor)
        menusettingsicon.setTextColor(ForegroundColor)

        menuanmeldebutton.setTextColor(ForegroundColor)
        menuanmeldeicon.setTextColor(ForegroundColor)

    }

}
