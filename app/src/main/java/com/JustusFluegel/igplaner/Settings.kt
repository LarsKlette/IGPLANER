@file:Suppress("PackageName")
package com.JustusFluegel.igplaner

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Switch
import android.widget.TextView

import com.JustusFluegel.general.Functions
import com.JustusFluegel.general.Menue

class Settings : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        Menue.Menu(this@Settings, false)

        (findViewById<View>(R.id.light_design) as Switch).setTextColor(Functions.getDesignTextcolor(this@Settings))
        (findViewById<View>(R.id.licence) as TextView).setTextColor(Functions.getDesignTextcolorDark(this@Settings))
        findViewById<View>(R.id.settings_background).setBackgroundResource(Functions.getDesignBackgroundImage(this@Settings))

        val lightdesign = findViewById<Switch>(R.id.light_design)

        lightdesign.setOnClickListener {
            Functions.setColors(lightdesign.isChecked, this@Settings)

            (findViewById<View>(R.id.light_design) as Switch).setTextColor(Functions.getDesignTextcolor(this@Settings))
            (findViewById<View>(R.id.licence) as TextView).setTextColor(Functions.getDesignTextcolorDark(this@Settings))
            findViewById<View>(R.id.settings_background).setBackgroundResource(Functions.getDesignBackgroundImage(this@Settings))
        }

    }
}
