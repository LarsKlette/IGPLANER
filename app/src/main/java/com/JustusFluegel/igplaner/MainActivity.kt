@file:Suppress("LocalVariableName", "PackageName")
package com.JustusFluegel.igplaner

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.ImageView
import android.widget.TextView
import com.JustusFluegel.general.Initialize
import com.JustusFluegel.html.ServerHttpConnection

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Initialize.startInitialize(this@MainActivity)

        val IGPlaner = findViewById<TextView>(R.id.IGPlanerTextView)
        IGPlaner.visibility = View.INVISIBLE
        val IGPlaner_Image = findViewById<ImageView>(R.id.IGPlaner_Image)

        val anim = TranslateAnimation(1000f, Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat())
        anim.duration = 1500
        anim.fillAfter = true

        IGPlaner_Image.startAnimation(anim)


        val handler = Handler()
        handler.postDelayed({
            IGPlaner.visibility = View.VISIBLE
            val anim2 = TranslateAnimation(Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat(), -1000f, Animation.ABSOLUTE.toFloat())
            anim2.duration = 2000
            anim2.fillAfter = true

            IGPlaner.startAnimation(anim2)
        }, 2000)

        val handler2 = Handler()
        handler2.postDelayed({
            startActivity(Intent(this@MainActivity, StandardActivity::class.java))
            finish()
        }, 5000)

    }


    fun internetAvailable(): Boolean {

        var result = false
        var exc = ""

        try {
            val conn = ServerHttpConnection("http://justusfluegel.ddnss.de/pass/pass_safe_auth.php")
            val res = conn.sendmultipleTextForResultinArray(arrayOf("CKSR"), arrayOf("true"))[0]
            result = res == "true"
        } catch (ex: Exception) {

            exc = ex.message!!

        }

        Log.d("internet:", java.lang.Boolean.toString(result) + "   |   " + exc)
        return result

    }

}
