@file:Suppress("PackageName", "PrivatePropertyName", "LocalVariableName", "MemberVisibilityCanBePrivate")
package com.JustusFluegel.igplaner

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.*
import com.JustusFluegel.general.Functions
import com.JustusFluegel.general.Menue
import com.JustusFluegel.html.ServerHttpConnection
import com.github.amlcurran.showcaseview.OnShowcaseEventListener
import com.github.amlcurran.showcaseview.ShowcaseView
import com.github.amlcurran.showcaseview.targets.ViewTarget

class Anmelden : AppCompatActivity() {

    private lateinit var AnmeldeBut: Button

    private lateinit var showCaseViewBenutzername: ShowcaseView.Builder
    private lateinit var showCaseViewPasswort: ShowcaseView.Builder
    private lateinit var showCaseViewBestaetigen: ShowcaseView.Builder
    private lateinit var showCaseViewAbmeldeBestaetigen: ShowcaseView.Builder


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anmelden)

        Menue.Menu(this@Anmelden, false)


        (findViewById<View>(R.id.Abmelden_Text) as TextView).setTextColor(Functions.getDesignTextcolor(this@Anmelden))
        (findViewById<View>(R.id.Anmelden_Text) as TextView).setTextColor(Functions.getDesignTextcolor(this@Anmelden))
        (findViewById<View>(R.id.Anmelden_Benutzer_Text) as TextView).setTextColor(Functions.getDesignTextcolor(this@Anmelden))
        (findViewById<View>(R.id.Anmelden_Passwort_Text) as TextView).setTextColor(Functions.getDesignTextcolor(this@Anmelden))

        (findViewById<View>(R.id.AnmeldeBestaetigungsButton) as Button).setTextColor(Functions.getDesignTextcolor(this@Anmelden))
        findViewById<View>(R.id.AnmeldeBestaetigungsButton).setBackgroundColor(Functions.getDesignBackground(this@Anmelden))
        (findViewById<View>(R.id.AbmeldeBestatigungsButton) as Button).setTextColor(Functions.getDesignTextcolor(this@Anmelden))
        findViewById<View>(R.id.AbmeldeBestatigungsButton).setBackgroundColor(Functions.getDesignBackground(this@Anmelden))

        findViewById<View>(R.id.anmelden_background).setBackgroundResource(Functions.getDesignBackgroundImage(this@Anmelden))


        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        val AnmeldePreferences = getSharedPreferences("Anmeldedaten", 0)
        val AnmeldePreferencesEditor = AnmeldePreferences.edit()


        val AnmeldeButton = findViewById<Button>(R.id.AnmeldeBestaetigungsButton)

        AnmeldeBut = AnmeldeButton

        val benutzername = findViewById<EditText>(R.id.Benutzername)
        val passwort = findViewById<EditText>(R.id.PasswortAnmelden)

        val error = findViewById<TextView>(R.id.FehlerText)

        val AbmeldeButton = findViewById<Button>(R.id.AbmeldeBestatigungsButton)
        val AnmeldeLayout = findViewById<FrameLayout>(R.id.Anmelden)
        val AbmeldeLayout = findViewById<FrameLayout>(R.id.Abmelden)


        if (Variables.angemeldet) {


            AnmeldeLayout.visibility = View.INVISIBLE
            AbmeldeLayout.visibility = View.VISIBLE


        }


        AnmeldeButton.setOnClickListener {
            if (Variables.SqlServerOn!! && isConnected(applicationContext)) {

                if (Variables.angemeldet) {

                    error.text = "Du bist bereits angemeldet!"
                    error.visibility = View.VISIBLE
                    error.setTextColor(ContextCompat.getColor(applicationContext,R.color.errorcolor))


                } else {

                    error.visibility = View.VISIBLE

                    error.text = "Bitte warten ..."

                    error.setTextColor(ContextCompat.getColor(applicationContext,R.color.textcolor))


                    val passw = passwort.text.toString()
                    val benut = benutzername.text.toString()


                    Thread(Runnable {
                        var l: String? = null
                        try {
                            l = login(passw, benut)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        if (l != "false") {

                            Variables.angemeldet = true

                            runOnUiThread {
                                error.setTextColor(ContextCompat.getColor(applicationContext,R.color.erfolgreichcolor))
                                Toast.makeText(this@Anmelden, "Angemeldet!", Toast.LENGTH_SHORT).show()
                            }


                            Variables.Klasse = Integer.parseInt(l)

                            Variables.Username = benut

                            AnmeldePreferencesEditor.putString("MyUsername", benut)
                            AnmeldePreferencesEditor.putString("MyPasword", passw)
                            AnmeldePreferencesEditor.putInt("Abgemeldet", 0)
                            AnmeldePreferencesEditor.apply()

                            finish()


                        } else {

                            runOnUiThread {
                                error.setTextColor(ContextCompat.getColor(applicationContext,R.color.errorcolor))
                                error.setText(R.string.FehlerBenutzername)
                            }

                        }
                    }).start()


                }
            } else {

                if (isConnected(applicationContext)) {

                    error.visibility = View.VISIBLE
                    error.setTextColor(ContextCompat.getColor(applicationContext,R.color.errorcolor))
                    error.text = "Der Anmeldeserver ist leider nicht an! Bitte die App neustarten um zu aktuallisieren!"

                } else {

                    error.visibility = View.VISIBLE
                    error.setTextColor(ContextCompat.getColor(applicationContext,R.color.errorcolor))
                    error.text = "Du brauchst eine Internetverbindung um dich anzumelden!"
                }
            }
        }

        AbmeldeButton.setOnClickListener {
            Variables.angemeldet = false
            Toast.makeText(this@Anmelden, "Abgemeldet!", Toast.LENGTH_SHORT).show()
            AnmeldePreferencesEditor.putInt("Abgemeldet", 1)
            AnmeldePreferencesEditor.apply()
            finish()
        }

        if (Variables.angemeldet) {

            showshowCaseViewAbmeldeBestaetigen()

        } else {

            showShowcaseViewBenutzername()

        }

    }

    fun showshowCaseViewAbmeldeBestaetigen() {

        showCaseViewAbmeldeBestaetigen = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.AbmeldeBestatigungsButton, this))
                .setContentTitle("Abmelden")
                .setContentText("Klicke hier, um dich Abzumelden.")
                .singleShot(97060973)
                .setStyle(R.style.ShowCaseViewStyle)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        //nächster Showcase

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewAbmeldeBestaetigen.build()

    }

    fun showshowCaseViewBestaetigen() {

        showCaseViewBestaetigen = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.AnmeldeBestaetigungsButton, this))
                .setContentTitle("Bestätigen")
                .setContentText("Klicke hier, um das Anmeldeformular absusenden und dich Anzumelden. Wenn du dch einmal angemeldet hast, wirst du weiterhin mit diesem Account automatisch angemeldet, wenn du die App startest.")
                .setStyle(R.style.ShowCaseViewStyle)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        //nächster Showcase

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewBestaetigen.build()

    }

    fun showShowcaseViewPasswort() {

        showCaseViewPasswort = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.PasswortAnmelden, this))
                .setContentTitle("Passwort")
                .setContentText("Tippe hier dein Passwort ein!")
                .setStyle(R.style.ShowCaseViewStyle)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showshowCaseViewBestaetigen()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewPasswort.build()

    }

    fun showShowcaseViewBenutzername() {

        showCaseViewBenutzername = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.Benutzername, this))
                .setContentTitle("Benutzername")
                .setContentText("Tippe hier deinen Benutzernamen ein!")
                .setStyle(R.style.ShowCaseViewStyle)
                .singleShot(97060971)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showShowcaseViewPasswort()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewBenutzername.build()

    }

    companion object {

        @Throws(Exception::class)
        fun login(pass: String, user: String): String {

            val conn = ServerHttpConnection("https://" + Variables.Domain + "/pass/login.php")
            val res = conn.sendmultipleTextForResultinArray(arrayOf("login", "pass", "user"), arrayOf("true", pass, user))

            return if (res.size != 0) {

                res[0]
            } else "false"

        }

        fun isConnected(context: Context): Boolean {

            val conMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val i = conMgr.activeNetworkInfo
            return i != null && i.isConnected && i.isAvailable

        }
    }

}
