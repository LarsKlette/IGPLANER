@file:Suppress("PackageName", "PrivatePropertyName", "LocalVariableName")
package com.JustusFluegel.igplaner

import android.content.res.Configuration
import android.graphics.Typeface
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.JustusFluegel.general.Functions
import com.JustusFluegel.general.Menue
import java.util.*

class Dienste : AppCompatActivity() {

    private val Dienstenamen = ArrayList<String>()
    private val DienstePersonenAnzahl = ArrayList<Int>()
    private val staticPersons = ArrayList<String>()
    private val anotherStaticPerson = ArrayList<String>()

    private lateinit var schrftart_symbole: Typeface


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dienste)

        Menue.Menu(this@Dienste, false)

        val err_container = findViewById<LinearLayout>(R.id.Err_Container)
        val err_img = findViewById<TextView>(R.id.Err_img)
        val err_text = findViewById<TextView>(R.id.Err_text)

        schrftart_symbole = Typeface.createFromAsset(assets, "Schriftart_Symbole/Symbols-Regular.ttf")

        if (Variables.Klasse == 38 || Variables.Klasse == 37 || Variables.Klasse == 36) {

            finish()
            Toast.makeText(applicationContext, "Die Oberstufe hat keine Dienste!", Toast.LENGTH_LONG).show()

        }

        val translationValue = 50

        //<editor-fold desc="Ladesymbol Anzeigen">
        err_img.text = "B"
        err_img.typeface = schrftart_symbole

        err_text.setText(R.string.text_wait_dienste_werden_abgerufen)

        err_container.translationY = 20f

        err_container.visibility = View.VISIBLE

        val anim_in = TranslateAnimation(Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat(), (dpToPx(translationValue) - dpToPx(translationValue * 2)).toFloat(), Animation.ABSOLUTE.toFloat())
        anim_in.duration = 800
        anim_in.fillAfter = true

        err_container.startAnimation(anim_in)
        //</editor-fold>


        Thread(Runnable {
            runOnUiThread {
                //<editor-fold desc="Elemente zum anzeigen der Dienste">


                val lay = findViewById<FrameLayout>(R.id.content)

                val vericalcase = LinearLayout(applicationContext)
                vericalcase.orientation = LinearLayout.VERTICAL

                for (i in Dienstenamen.indices) {
                    val element = LinearLayout(applicationContext)
                    val elementparams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    elementparams.setMargins(dpToPx(16), dpToPx(16), dpToPx(16), dpToPx(16))
                    element.setPadding(dpToPx(8), dpToPx(8), dpToPx(8), dpToPx(8))
                    elementparams.gravity = Gravity.CENTER
                    element.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.background))
                    element.layoutParams = elementparams
                    element.orientation = LinearLayout.VERTICAL

                    val ueberschrift = TextView(applicationContext)
                    ueberschrift.textSize = 30f
                    ueberschrift.gravity = Gravity.CENTER_HORIZONTAL
                    ueberschrift.text = Dienstenamen[i]
                    ueberschrift.setTextColor(ContextCompat.getColor(applicationContext,R.color.textcolor))
                    element.addView(ueberschrift)

                    val underline = ConstraintLayout(applicationContext)
                    val underlineparams = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(1))
                    underlineparams.setMargins(dpToPx(8), dpToPx(8), dpToPx(8), 0)
                    underline.layoutParams = underlineparams
                    underline.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.textcolor))
                    element.addView(underline)

                    val personenanzeige = LinearLayout(applicationContext)
                    val personenanzeigeparams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    personenanzeigeparams.gravity = Gravity.CENTER_HORIZONTAL
                    personenanzeige.orientation = LinearLayout.HORIZONTAL
                    personenanzeigeparams.setMargins(0, dpToPx(12), 0, dpToPx(12))
                    personenanzeige.layoutParams = personenanzeigeparams
                    val personen = TextView(applicationContext)
                    personen.text = "A" //die Figur
                    personen.typeface = schrftart_symbole
                    personen.setTextColor(ContextCompat.getColor(applicationContext,R.color.textcolor))
                    personenanzeige.addView(personen)
                    val personenanzahl = TextView(applicationContext)
                    personenanzahl.setTextColor(ContextCompat.getColor(applicationContext,R.color.textcolor))

                    when {
                        staticPersons[i] == "d49b1602-b959-40c7-bd00-5c3e512b5a31" -> personenanzahl.text = "  " + Integer.toString(DienstePersonenAnzahl[i])
                        anotherStaticPerson[i] == "d49b1602-b959-40c7-bd00-5c3e512b5a31" -> personenanzahl.text = "  " + Integer.toString(DienstePersonenAnzahl[i]) + "  +  " + staticPersons[i]
                        else -> personenanzahl.text = "  " + Integer.toString(DienstePersonenAnzahl[i]) + "  +  " + staticPersons[i] + " oder " + anotherStaticPerson[i]
                    }

                    personenanzeige.addView(personenanzahl)
                    element.addView(personenanzeige)

                    val underline2 = ConstraintLayout(applicationContext)
                    val underline2params = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dpToPx(1))
                    underline2params.setMargins(dpToPx(64), 0, dpToPx(64), dpToPx(8))
                    underline2.layoutParams = underline2params
                    underline2.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.textcolor))
                    element.addView(underline2)


                    val infotext = TextView(applicationContext)
                    val infotextparams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    infotextparams.setMargins(0, 0, 0, dpToPx(10))
                    if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {

                        infotext.text = "Personen, die Dienst haben:"

                    } else if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {

                        infotext.text = "Diese Woche haben folgende Personen Dienst:"

                    }


                    infotextparams.gravity = Gravity.CENTER_HORIZONTAL

                    infotext.layoutParams = infotextparams
                    infotext.gravity = Gravity.CENTER_HORIZONTAL
                    infotext.setTextColor(ContextCompat.getColor(applicationContext,R.color.textcolor))
                    element.addView(infotext)

                    val personendiediensthaben = TextView(applicationContext)
                    val personendiediensthabenparams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    personendiediensthabenparams.gravity = Gravity.CENTER_HORIZONTAL
                    personendiediensthaben.gravity = Gravity.CENTER_HORIZONTAL
                    personendiediensthabenparams.setMargins(0, 0, 0, dpToPx(8))
                    personendiediensthaben.layoutParams = personendiediensthabenparams
                    personendiediensthaben.setTextColor(ContextCompat.getColor(applicationContext,R.color.hervorgehoben))

                    val personendiediensthabentext: String = dienste[i]


                    personendiediensthaben.text = personendiediensthabentext
                    element.addView(personendiediensthaben)


                    vericalcase.addView(element)
                }

                lay.addView(vericalcase)


                //</editor-fold>
                //<editor-fold desc="Ladesymbol Ausblenden">
                err_container.y = err_container.y - dpToPx(translationValue)

                val anim_out = TranslateAnimation(Animation.ABSOLUTE.toFloat(), Animation.ABSOLUTE.toFloat(), dpToPx(translationValue).toFloat(), Animation.ABSOLUTE.toFloat())
                anim_out.duration = 800
                anim_out.fillAfter = true

                err_container.startAnimation(anim_out)

                err_container.postDelayed({ err_container.visibility = View.INVISIBLE }, 800)
                //</editor-fold>
            }
        }).start()


    }


    private fun dpToPx(dp: Int): Int {
        return Functions.dpToPx(dp , this)
    }

    companion object {
        private val dienste = ArrayList<String>()
    }

}
