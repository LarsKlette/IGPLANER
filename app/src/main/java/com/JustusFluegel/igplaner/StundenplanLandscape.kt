@file:Suppress("PackageName", "LocalVariableName")
package com.JustusFluegel.igplaner

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.JustusFluegel.general.Functions
import com.JustusFluegel.general.Menue
import com.github.amlcurran.showcaseview.OnShowcaseEventListener
import com.github.amlcurran.showcaseview.ShowcaseView
import com.github.amlcurran.showcaseview.targets.ViewTarget
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException

class StundenplanLandscape : AppCompatActivity() {

    private lateinit var showCaseViewStunden: ShowcaseView.Builder
    private lateinit var showCaseViewTage: ShowcaseView.Builder
    private lateinit var showCaseViewFach: ShowcaseView.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stundenplan_landscape)

        //<editor-fold desc="SetLayoutColors" />
        findViewById<View>(R.id.Überschriften).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Stundenplan_Landscape_background).setBackgroundResource(Functions.getDesignBackgroundImage(this@StundenplanLandscape))

        (findViewById<View>(R.id.Stunden_Ueberschrift) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag_Ueberschrift) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag_Ueberschrift) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch_Ueberschrift) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag_Ueberschrift) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag_Ueberschrift) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))

        (findViewById<View>(R.id.Montag1Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag2Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag3Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag4Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag5Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag6Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag7Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag8Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Montag9Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))

        (findViewById<View>(R.id.Dienstag1Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag2Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag3Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag4Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag5Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag6Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag7Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag8Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Dienstag9Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))

        (findViewById<View>(R.id.Mittwoch1Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch2Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch3Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch4Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch5Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch6Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch7Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch8Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Mittwoch9Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))

        (findViewById<View>(R.id.Donnerstag1Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag2Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag3Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag4Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag5Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag6Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag7Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag8Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Donnerstag9Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))

        (findViewById<View>(R.id.Freitag1Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag2Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag3Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag4Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag5Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag6Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag7Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag8Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Freitag9Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))

        (findViewById<View>(R.id.Text1Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text2Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text3Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text4Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text5Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text6Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text7Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text8Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))
        (findViewById<View>(R.id.Text9Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanLandscape))

        findViewById<View>(R.id.Text1Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text2Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text3Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text4Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text5Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text6Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text7Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text8Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        findViewById<View>(R.id.Text9Stunde).setBackgroundColor(Functions.getDesignBackground(this@StundenplanLandscape))
        //</editor-fold>

        val StundenplanPreferences = getSharedPreferences("Anmeldedaten", 0)
        val StundenplanPreferencesEditor = StundenplanPreferences.edit()

        if (!Variables.angemeldet && !Variables.pseudoangemeldet) {

            finish()
            Toast.makeText(this, "Du bist nicht Angemeldet, obwohl du online bist. Wenn nicht, starte die App neu!", Toast.LENGTH_SHORT).show()

        } else {
            if (Variables.angemeldet && Variables.Klasse != Integer.MAX_VALUE && Anmelden.isConnected(this)) {

                try {
                    val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                    StrictMode.setThreadPolicy(policy)


                    Class.forName("com.mysql.jdbc.Driver")
                    val conn: Connection = DriverManager.getConnection("jdbc:mysql://" + Variables.SqlDomain + "/stundenplane?verifyServerCertificate=false&useSSL=true&requireSSL=true", Variables.SqlUsername, Variables.SqlPassword)

                    val stmt = conn.createStatement()

                    val res: ResultSet

                    res = if (Variables.Klasse == 38 || Variables.Klasse == 37 || Variables.Klasse == 36) {

                        stmt.executeQuery("SELECT * FROM `" + Variables.klassen[Variables.Klasse] + "` WHERE Benutzername='" + Variables.Username + "'")

                    } else {

                        stmt.executeQuery("SELECT * FROM `" + Variables.klassen[Variables.Klasse] + "`")

                    }


                    while (res.next()) {

                        val Montagr = res.getString("Montag")
                        val Dienstagr = res.getString("Dienstag")
                        val Mittwochr = res.getString("Mittwoch")
                        val Donnerstagr = res.getString("Donnerstag")
                        val Freitagr = res.getString("Freitag")

                        val Montagresult = Montagr.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        val Dienstagresult = Dienstagr.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        val Mittwochresult = Mittwochr.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        val Donnerstagresult = Donnerstagr.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        val Freitagresult = Freitagr.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                        for (i in 0..8) {

                            Montag[i] = Montagresult[i]
                            Dienstag[i] = Dienstagresult[i]
                            Mittwoch[i] = Mittwochresult[i]
                            Donnerstag[i] = Donnerstagresult[i]
                            Freitag[i] = Freitagresult[i]

                            StundenplanPreferencesEditor.putString("Montag" + Integer.toString(i), Montagresult[i])
                            StundenplanPreferencesEditor.putString("Dienstag" + Integer.toString(i), Dienstagresult[i])
                            StundenplanPreferencesEditor.putString("Mittwoch" + Integer.toString(i), Mittwochresult[i])
                            StundenplanPreferencesEditor.putString("Donnerstag" + Integer.toString(i), Donnerstagresult[i])
                            StundenplanPreferencesEditor.putString("Freitag" + Integer.toString(i), Freitagresult[i])

                        }

                        StundenplanPreferencesEditor.apply()

                    }


                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                } catch (e: SQLException) {
                    e.printStackTrace()
                }

            }

            if (Variables.pseudoangemeldet && !Anmelden.isConnected(applicationContext)) {

                for (i in 0..8) {

                    Montag[i] = StundenplanPreferences.getString("Montag" + Integer.toString(i), "KeineStundeHinterlegt")
                    Dienstag[i] = StundenplanPreferences.getString("Dienstag" + Integer.toString(i), "KeineStundeHinterlegt")
                    Mittwoch[i] = StundenplanPreferences.getString("Mittwoch" + Integer.toString(i), "KeineStundeHinterlegt")
                    Donnerstag[i] = StundenplanPreferences.getString("Donnerstag" + Integer.toString(i), "KeineStundeHinterlegt")
                    Freitag[i] = StundenplanPreferences.getString("Freitag" + Integer.toString(i), "KeineStundeHinterlegt")

                }

            }

            //<editor-fold desc="Displayabhaenig aendern">
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                startActivity(Intent(this@StundenplanLandscape, StundenplanProitrait::class.java))
                finish()
            } else {
                //</editor-fold>

                //<editor-fold desc="Variablen"<

                val Montag1 = findViewById<TextView>(R.id.Montag1Stunde)
                val Montag2 = findViewById<TextView>(R.id.Montag2Stunde)
                val Montag3 = findViewById<TextView>(R.id.Montag3Stunde)
                val Montag4 = findViewById<TextView>(R.id.Montag4Stunde)
                val Montag5 = findViewById<TextView>(R.id.Montag5Stunde)
                val Montag6 = findViewById<TextView>(R.id.Montag6Stunde)
                val Montag7 = findViewById<TextView>(R.id.Montag7Stunde)
                val Montag8 = findViewById<TextView>(R.id.Montag8Stunde)
                val Montag9 = findViewById<TextView>(R.id.Montag9Stunde)

                val Dienstag1 = findViewById<TextView>(R.id.Dienstag1Stunde)
                val Dienstag2 = findViewById<TextView>(R.id.Dienstag2Stunde)
                val Dienstag3 = findViewById<TextView>(R.id.Dienstag3Stunde)
                val Dienstag4 = findViewById<TextView>(R.id.Dienstag4Stunde)
                val Dienstag5 = findViewById<TextView>(R.id.Dienstag5Stunde)
                val Dienstag6 = findViewById<TextView>(R.id.Dienstag6Stunde)
                val Dienstag7 = findViewById<TextView>(R.id.Dienstag7Stunde)
                val Dienstag8 = findViewById<TextView>(R.id.Dienstag8Stunde)
                val Dienstag9 = findViewById<TextView>(R.id.Dienstag9Stunde)

                val Mittwoch1 = findViewById<TextView>(R.id.Mittwoch1Stunde)
                val Mittwoch2 = findViewById<TextView>(R.id.Mittwoch2Stunde)
                val Mittwoch3 = findViewById<TextView>(R.id.Mittwoch3Stunde)
                val Mittwoch4 = findViewById<TextView>(R.id.Mittwoch4Stunde)
                val Mittwoch5 = findViewById<TextView>(R.id.Mittwoch5Stunde)
                val Mittwoch6 = findViewById<TextView>(R.id.Mittwoch6Stunde)
                val Mittwoch7 = findViewById<TextView>(R.id.Mittwoch7Stunde)
                val Mittwoch8 = findViewById<TextView>(R.id.Mittwoch8Stunde)
                val Mittwoch9 = findViewById<TextView>(R.id.Mittwoch9Stunde)

                val Donnerstag1 = findViewById<TextView>(R.id.Donnerstag1Stunde)
                val Donnerstag2 = findViewById<TextView>(R.id.Donnerstag2Stunde)
                val Donnerstag3 = findViewById<TextView>(R.id.Donnerstag3Stunde)
                val Donnerstag4 = findViewById<TextView>(R.id.Donnerstag4Stunde)
                val Donnerstag5 = findViewById<TextView>(R.id.Donnerstag5Stunde)
                val Donnerstag6 = findViewById<TextView>(R.id.Donnerstag6Stunde)
                val Donnerstag7 = findViewById<TextView>(R.id.Donnerstag7Stunde)
                val Donnerstag8 = findViewById<TextView>(R.id.Donnerstag8Stunde)
                val Donnerstag9 = findViewById<TextView>(R.id.Donnerstag9Stunde)

                val Freitag1 = findViewById<TextView>(R.id.Freitag1Stunde)
                val Freitag2 = findViewById<TextView>(R.id.Freitag2Stunde)
                val Freitag3 = findViewById<TextView>(R.id.Freitag3Stunde)
                val Freitag4 = findViewById<TextView>(R.id.Freitag4Stunde)
                val Freitag5 = findViewById<TextView>(R.id.Freitag5Stunde)
                val Freitag6 = findViewById<TextView>(R.id.Freitag6Stunde)
                val Freitag7 = findViewById<TextView>(R.id.Freitag7Stunde)
                val Freitag8 = findViewById<TextView>(R.id.Freitag8Stunde)
                val Freitag9 = findViewById<TextView>(R.id.Freitag9Stunde)
                //</editor-fold>

                Menue.Menu(this@StundenplanLandscape, false)

                //<editor-fold desc="Arrays auslesen">

                Montag1.text = Montag[0]
                Montag2.text = Montag[1]
                Montag3.text = Montag[2]
                Montag4.text = Montag[3]
                Montag5.text = Montag[4]
                Montag6.text = Montag[5]
                Montag7.text = Montag[6]
                Montag8.text = Montag[7]
                Montag9.text = Montag[8]
                Dienstag1.text = Dienstag[0]
                Dienstag2.text = Dienstag[1]
                Dienstag3.text = Dienstag[2]
                Dienstag4.text = Dienstag[3]
                Dienstag5.text = Dienstag[4]
                Dienstag6.text = Dienstag[5]
                Dienstag7.text = Dienstag[6]
                Dienstag8.text = Dienstag[7]
                Dienstag9.text = Dienstag[8]
                Mittwoch1.text = Mittwoch[0]
                Mittwoch2.text = Mittwoch[1]
                Mittwoch3.text = Mittwoch[2]
                Mittwoch4.text = Mittwoch[3]
                Mittwoch5.text = Mittwoch[4]
                Mittwoch6.text = Mittwoch[5]
                Mittwoch7.text = Mittwoch[6]
                Mittwoch8.text = Mittwoch[7]
                Mittwoch9.text = Mittwoch[8]
                Donnerstag1.text = Donnerstag[0]
                Donnerstag2.text = Donnerstag[1]
                Donnerstag3.text = Donnerstag[2]
                Donnerstag4.text = Donnerstag[3]
                Donnerstag5.text = Donnerstag[4]
                Donnerstag6.text = Donnerstag[5]
                Donnerstag7.text = Donnerstag[6]
                Donnerstag8.text = Donnerstag[7]
                Donnerstag9.text = Donnerstag[8]
                Freitag1.text = Freitag[0]
                Freitag2.text = Freitag[1]
                Freitag3.text = Freitag[2]
                Freitag4.text = Freitag[3]
                Freitag5.text = Freitag[4]
                Freitag6.text = Freitag[5]
                Freitag7.text = Freitag[6]
                Freitag8.text = Freitag[7]
                Freitag9.text = Freitag[8]

                //</editor-fold>

                showShowcaseViewTage()

            }

        }
    }

    fun showShowcaseViewFach() {

        showCaseViewFach = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.Montag5Stunde, this))
                .setContentTitle("Fach")
                .setContentText("Hier Kannst du dein Fach ablesen.")
                .setStyle(R.style.ShowCaseViewStyle)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        //nächster Showcase

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewFach.build()

    }

    fun showShowcaseViewStunden() {

        showCaseViewStunden = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.Text5Stunde, this))
                .setContentTitle("Stunden")
                .setContentText("Hier kannst du gucken, welche Reihe welche Stunde ist.")
                .setStyle(R.style.ShowCaseViewStyle)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showShowcaseViewFach()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewStunden.build()

    }

    private fun showShowcaseViewTage() {

        showCaseViewTage = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.Überschriften, this))
                .setContentTitle("Tage")
                .setContentText("Hier sind die Spalten mit den Tagen beschriftet.")
                .setStyle(R.style.ShowCaseViewStyle)
                .singleShot(710331953)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showShowcaseViewStunden()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewTage.build()

    }

    companion object {

        val Montag = arrayOf("", "", "", "", "", "", "", "", "")
        val Dienstag = arrayOf("", "", "", "", "", "", "", "", "")
        val Mittwoch = arrayOf("", "", "", "", "", "", "", "", "")
        val Donnerstag = arrayOf("", "", "", "", "", "", "", "", "")
        val Freitag = arrayOf("", "", "", "", "", "", "", "", "")
    }

}
