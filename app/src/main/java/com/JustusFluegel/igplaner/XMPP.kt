package com.JustusFluegel.igplaner

import android.content.Intent
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.text.emoji.EmojiCompat
import android.support.text.emoji.bundled.BundledEmojiCompatConfig
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.Spinner
import android.widget.TextView

import com.JustusFluegel.chat.Chat
import com.JustusFluegel.chat.ChatView
import com.JustusFluegel.chat.MediaButtons
import com.JustusFluegel.chat.XMPPDatabase
import com.JustusFluegel.emojii.EmojiiButton
import com.JustusFluegel.general.Functions
import com.JustusFluegel.general.Menue
import com.JustusFluegel.instances.KeyboardEvents
import com.github.data5tream.emojilib.EmojiEditText

import java.util.ArrayList

class XMPP : KeyboardEvents() {

    internal var disablesystemclick = false

    internal var currentlyselected: String? = null
    internal var contacts_visible: ArrayList<String>? = null
    internal var contacts: ArrayList<String>? = null
    internal var contacts_visible_array: Array<String>? = null
    internal var acual_user_name: String? = null
    internal var buttons: MediaButtons
    internal var emojii_button: EmojiiButton

    //<editor-fold desc="Get files returned from dialogs"
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        buttons.requestOnActivityResult(requestCode, resultCode, data)
    }

    override fun onUserInteraction() {
        buttons.requestOnUserInteraction()
    }

    override fun onShowKeyboard(keyboardHeight: Int) {
        emojii_button.requestOnShowKeyboard()
    }

    override fun onHideKeyboard() {
        emojii_button.requestOnCloseKeyboard()
    }
    //</editor-fold>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_xmpp)

        //<editor-fold desc="Initialisize"
        Menue.Menu(this@XMPP, false)
        attachKeyboardListeners()
        EmojiCompat.init(BundledEmojiCompatConfig(applicationContext))
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        //</editor-fold>

        //<editor-fold desc="Define Variables" >
        val emojiis = findViewById<Button>(R.id.Emojiis)
        val symbols = Typeface.createFromAsset(assets, "Schriftart_Symbole/Symbols-Regular.ttf")
        //</editor-fold>

        //<editor-fold desc="Set Design"
        findViewById<View>(R.id.Controls).setBackgroundColor(Functions.getDesignMenue(this@XMPP))
        findViewById<View>(R.id.User_data).setBackgroundColor(Functions.getDesignMenue(this@XMPP))
        (findViewById<View>(R.id.User_Name) as TextView).setTextColor(Functions.getDesignTextcolor(this@XMPP))
        findViewById<View>(R.id.Contact_XMPP_Background).setBackgroundResource(Functions.getDesignBackgroundImage(this@XMPP))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            (findViewById<View>(R.id.SelectContact) as Spinner).setPopupBackgroundDrawable(ColorDrawable(Functions.getDesignMenue(this@XMPP)))
        }
        //</editor-fold>

        XMPPDatabase()

        val chatview = ChatView(this, findViewById<View>(R.id.Chat_Content) as ScrollView, R.color.chat_bubble, R.color.warn)

        val chattext = arrayOf("Hi", "Ho", "He", "Thomas", "Justus", "Nicole")
        val chatmyself = arrayOf(true, true, false, false, true, false)
        val sender = arrayOf<String>("Marlene", null, "Nicole", "Thomas", null, "Justus")

        for (z in chattext.indices) {

            val ele = TextView(this)
            ele.text = chattext[z]
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.setMargins(0, 0, 0, 0)
            ele.layoutParams = params

            val myself = chatmyself[z]

            chatview.append(false, myself, ele, sender[z])

        }

        chatview.apply()

        //<editor-fold desc="Define MediaButtons"
        buttons = MediaButtons(this@XMPP, findViewById<View>(R.id.Document) as Button, findViewById<View>(R.id.Camera) as Button, emojiis, findViewById(R.id.rootLayout), findViewById<View>(R.id.Chat_Text) as EmojiEditText, symbols)
        emojii_button = buttons.emojiButton
        //</editor-fold>

        val contact = SelectContact(findViewById<View>(R.id.SelectContact) as Spinner, findViewById<View>(R.id.User_Name) as TextView, this@XMPP, findViewById(R.id.User_data), findViewById(R.id.User_Name), findViewById(R.id.User_Image))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            contact.setDropDownViewResource(Functions.getDesignSppinnerStyle(this@XMPP))
        } else {

            contact.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        contact.selectChat(Chat(this@XMPP))

        contact.addContact("Viktor")
        contact.addContact("Miriam")
        contact.addContact("Bendix")
        contact.addContact("Carlotta")
        contact.addContact("Christian")
        contact.addContact("Englandfahrt")

        contact.addContactApply()

        contact.save()
        contact.show()

        /*
        final Spinner dropdown = findViewById(R.id.SelectContact);

        ArrayList<String> contacts_unsorted = new ArrayList<>();

        contacts_unsorted.add("Viktor");
        contacts_unsorted.add("Miriam");
        contacts_unsorted.add("Bendix");
        contacts_unsorted.add("Englandfahrt");

        contacts_visible = new ArrayList<>();
        contacts = new ArrayList<>();

        SharedPreferences lastselected = getSharedPreferences("XMPP", 0);
        String UName = lastselected.getString("Username", contacts_unsorted.get(0));

        for (String contact : contacts_unsorted) {
            if (!contact.equals(UName)) {
                contacts.add(contact);
            } else {
                currentlyselected = contact;
            }
        }

        contacts_visible.add("");
        contacts_visible.addAll(contacts);
        for (String cont : contacts_visible) {

            Log.d("Contacts", cont);

        }

        contacts_visible_array = new String[contacts_visible.size()];
        contacts_visible_array = contacts_visible.toArray(contacts_visible_array);

        final CustomAdapter dropdownadapter = new CustomAdapter(XMPP.this, android.R.layout.simple_spinner_item, contacts_visible_array, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            dropdownadapter.setDropDownViewResource(Functions.getDesignSppinnerStyle(XMPP.this));
        } else {

            dropdownadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }
        dropdown.setAdapter(dropdownadapter);

        final Button User_Image = findViewById(R.id.User_Image);
        final Button User_Name = findViewById(R.id.User_Name);
        final LinearLayout User_Data = findViewById(R.id.User_data);

        User_Name.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    dropdown.setDropDownWidth(User_Data.getWidth());
                }

                dropdown.performClick();

                dropdown.setSelection(0);
                dropdown.getSelectedView().setVisibility(View.INVISIBLE);


            }

        });

        User_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    dropdown.setDropDownWidth(User_Data.getWidth());
                }

                dropdown.performClick();

                dropdown.setSelection(0);
                dropdown.getSelectedView().setVisibility(View.INVISIBLE);

            }
        });

        User_Name.setText(currentlyselected);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (disablesystemclick) {

                    SharedPreferences lastselected = getSharedPreferences("XMPP", 0);
                    SharedPreferences.Editor lastselectededitor = lastselected.edit();
                    lastselectededitor.putString("Username", contacts_visible.get(i));
                    lastselectededitor.apply();

                    User_Name.setText(contacts_visible.get(i));
                    acual_user_name = contacts_visible.get(i);

                    String currentlyselected_save = currentlyselected;
                    currentlyselected = contacts_visible.get(i);
                    contacts_visible.remove(i);
                    contacts_visible.add(i, currentlyselected_save);

                    contacts_visible_array = contacts_visible.toArray(contacts_visible_array);

                    final CustomAdapter dropdownadapter_new = new CustomAdapter(XMPP.this, android.R.layout.simple_spinner_item, contacts_visible_array, 0);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        dropdownadapter_new.setDropDownViewResource(Functions.getDesignSppinnerStyle(XMPP.this));
                    } else {

                        dropdownadapter_new.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    }
                    dropdown.setAdapter(dropdownadapter_new);

                    disablesystemclick = false;

                } else {
                    disablesystemclick = true;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        */

        (findViewById<View>(R.id.User_Image) as TextView).typeface = symbols

    }

}
