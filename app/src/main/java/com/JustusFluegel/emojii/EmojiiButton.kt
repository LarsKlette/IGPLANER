@file:Suppress("PackageName", "LocalVariableName")
package com.JustusFluegel.emojii

import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import com.JustusFluegel.chat.Global
import com.JustusFluegel.general.Functions
import com.JustusFluegel.igplaner.R
import com.github.data5tream.emojilib.EmojiEditText
import com.github.data5tream.emojilib.EmojiGridView
import com.github.data5tream.emojilib.EmojiPopup

class EmojiiButton(activity: Activity, private val emoji_button: Button, input: EmojiEditText, rootLayout: View) {

    private val emojiis: Button = emoji_button

    init {

        val popup = EmojiPopup(rootLayout, activity, ContextCompat.getColor( activity.applicationContext , R.color.brand))


        val standard = emojiis.typeface

        val symbols = Typeface.createFromAsset(activity.assets, "Schriftart_Symbole/Symbols-Regular.ttf")


        input.requestFocus()

        popup.setSizeForSoftKeyboard()

        popup.setOnDismissListener {
            emojiis.typeface = symbols
            emojiis.text = "D"
            emojiis.setPadding(0, 0, 0, 0)
            emojiis.textSize = 24f
        }


        popup.setOnSoftKeyboardOpenCloseListener(object : EmojiPopup.OnSoftKeyboardOpenCloseListener {

            override fun onKeyboardOpen(keyBoardHeight: Int) {

            }

            override fun onKeyboardClose() {
                if (popup.isShowing)
                    popup.dismiss()
            }
        })

        popup.setOnEmojiconClickedListener(EmojiGridView.OnEmojiconClickedListener { emojicon ->
            if (emojicon == null) {
                return@OnEmojiconClickedListener
            }

            val start = input.selectionStart
            val end = input.selectionEnd
            if (start < 0) {
                input.append(emojicon.emoji)
            } else {
                input.text.replace(Math.min(start, end),
                        Math.max(start, end), emojicon.emoji, 0,
                        emojicon.emoji.length)
            }
        })

        popup.setOnEmojiconBackspaceClickedListener {
            val event = KeyEvent(
                    0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL)
            input.dispatchKeyEvent(event)
        }

        emojiis.setOnClickListener {
            val handler = Handler()

            handler.postDelayed({
                var Do = true

                try {
                    Class.forName("com.JustusFluegel.chat.Global")
                    Do = Global.openemoji
                    Global.openemoji = true
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                }

                if (Do) {

                    if (!popup.isShowing) {

                        if (popup.isKeyBoardOpen!!) {
                            popup.showAtBottom()

                            emojiis.typeface = standard
                            emojiis.text = "A"
                            emojiis.setPadding(0, 0, 0, Functions.dpToPx(5, activity))
                            emojiis.textSize = 20f
                        } else {
                            input.isFocusableInTouchMode = true
                            input.requestFocus()
                            popup.showAtBottomPending()
                            val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            inputMethodManager.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT)
                            emojiis.typeface = standard
                            emojiis.text = "A"
                            emojiis.setPadding(0, 0, 0, Functions.dpToPx(5, activity))
                            emojiis.textSize = 20f


                        }
                    } else {
                        popup.dismiss()
                    }
                }
            }, 2)
        }
    }

    fun requestOnShowKeyboard() {
        emoji_button.visibility = View.VISIBLE
    }

    fun requestOnCloseKeyboard() {
        emoji_button.visibility = View.GONE
    }

}
